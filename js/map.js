var App = App || {}

App.spreadsheetUrl = 'https://spreadsheets.google.com/feeds/list/1Bx--tchrk2aTPw4yk7sWfEGL8Sj9N0U677bxhx_JkgI/1/public/full?alt=json';
mapboxgl.accessToken = 'pk.eyJ1Ijoib2tmbiIsImEiOiJjaXlrOW5yczgwMDEzMnlwaWd2ZzF6MDQ3In0.2UJlkR69zbu4-3YRJJgN5w';

App.popupContent = "<div class='popup'>" +
"<h2>{{properties.name}}</h2>" +
"<div class='place'>{{properties.place}}</div>" +
"<div class='message'>{{properties.message}}</div>" +
"</div>";

App.popupTemplate = Mustache.render(App.popupContent);


var map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/mapbox/streets-v9',
  center: [0,0],
  zoom: 1.3,
  minZoom: 1.3
});

map.addControl(new mapboxgl.NavigationControl(), 'top-right');

map.on('load', function() {
  mapboxgl.util.getJSON(App.spreadsheetUrl, function(err, data) {
    var geojson = {
      type: 'FeatureCollection',
      features: []
    };

    data.feed.entry.forEach(function(d) {
      var lat = Number(d.gsx$latitude.$t),
          lng = Number(d.gsx$longitude.$t),
          name = d.gsx$yourname.$t,
          message = d.gsx$yourmessagetomor.$t,
          place = d.gsx$whereintheworldareyou.$t;

      if (lng && lat && name) {
        lat = lat + (0.02*Math.random()-0.01);
        lng = lng + (0.02*Math.random()-0.01)/Math.cos(lat/2/Math.PI);
        geojson.features.push({
          type: 'Feature',
          properties: {
            name: name,
			message: message,
			place: place,
            icon: "triangle",
          },
          geometry: {
            type: 'Point',
            coordinates: [lng, lat]
          }
        });
      }
    });

    map.addSource("people", {
      "type": "geojson",
      data: geojson,
      cluster: true,
      clusterMaxZoom: 14 // Max zoom to cluster points on
    });

    map.addLayer({
        id: "clusters",
        type: "circle",
        source: "people",
        filter: ["has", "point_count"],
        paint: {
            "circle-color": {
                property: "point_count",
                type: "interval",
                stops: [
			      [2, '#0086FF'], //blue
			      [10, '#FF4900'], //red
			      [20, '#FFB900'] //yellow
                ]
            },
            "circle-radius": {
                property: "point_count",
                type: "interval",
                stops: [
                    [2, 20],
                    [10, 30],
                    [20, 40]
                ]
            }
        }
    });

    map.addLayer({
        id: "cluster-count",
        type: "symbol",
        source: "people",
        filter: ["has", "point_count"],
        layout: {
            "text-field": "{point_count_abbreviated}",
            "text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Bold"],
            "text-size": 12
        }
    });

    map.addLayer({
        id: "unclustered-point",
        type: "symbol",
        source: "people",
        filter: ["!has", "point_count"],
		layout: {
		  "icon-image": "{icon}-15",
		  "text-size": 12,
		  "text-field": "{name}",
		  "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
		  "text-offset": [0, 0.6],
		  "text-anchor": "top"
		},
		paint: {
		  "text-color": "#fff",
		  "text-halo-color": "#000",
		  "text-halo-width": 1
		}
    });

	// Hack because MapBox doesn't allow to style the cursor on markers!
	map.on('mousemove', function (e) {
		var features = map.queryRenderedFeatures(e.point, {layers: ['unclustered-point']})
		if (features) {	
			map.getCanvas().style.cursor = features.length ? 'pointer' : '';
		}
	});

    map.on('click', function (e) {
      var features = map.queryRenderedFeatures(e.point, { layers: ['unclustered-point'] });
      if (!features.length) {
        return;
      } else {
        var feature = features[0];
        var popup = new mapboxgl.Popup()
            .setLngLat(feature.geometry.coordinates)
            .setHTML(Mustache.render(App.popupContent, feature))
            .addTo(map);
      }
    });
  });
});
